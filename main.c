#include <stdio.h>
#include <glib.h>
#include <gmodule.h>

int main (int argc, char** argv) {
    //const gchar *str = "This is an emoij: 😀 and a special character: ç";
    // https://stackoverflow.com/questions/8873973/defining-utf-16be-strings-in-c
    const gchar str[] = { 0, 5, 0, 'M', 0, 'y', 0, 'S', 0, 't', 0, 'r' };
    gssize len = strlen(str);
    //const gchar *to_codeset = "UTF-8//TRANSLIT";
    const gchar *to_codeset = "UTF-8";
    const gchar *from_codeset = "UTF16BE";
    gsize *bytes_read = NULL;
    gsize *bytes_written = NULL;
    GError *error = NULL;

    gchar *converted = g_convert (str, len, to_codeset, from_codeset,
                                  bytes_read, bytes_written, &error);

    if (!converted || error) {
        if(error)
            printf("%s\n", error->message);
        printf ("Conversion failed!\n");
        return 1;
    }

    printf ("OK!\n");
    printf ("Converted to: %s\n", converted);
    return 0;
}
