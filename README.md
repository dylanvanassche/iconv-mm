# convert-test

A simple piece of code to test and debug `iconv` issues on Alpine Linux.

## Build

- Make sure you have `glib-dev`, `make`, `gcc` and `musl-dev` installed
- Run `make`
- `./convert-test`


